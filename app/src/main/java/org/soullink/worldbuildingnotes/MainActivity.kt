package org.soullink.worldbuildingnotes

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import org.soullink.worldbuildingnotes.ui.theme.WorldbuildingNotesTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            WorldbuildingNotesTheme {
                // A surface container using the 'background' color from the theme
                MainAppScaffold()
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    WorldbuildingNotesTheme {
        MainAppScaffold()
    }
}